/*
 * File: main.c
 * Creator: George Ferguson
 * Contributor: Muskaan Mendiratta
 * Created: Mon Nov 28 14:11:17 2016
 * Edited: Wed Dec 5 2018
 * Time-stamp: <Tue Jul 17 16:02:29 EDT 2018 ferguson>
 */
#include <stdio.h>
#include <stdlib.h>
#include "Circuit.h"

/**
 * Two AND gates connected to make a 3-input AND circuit.
 */
static Circuit* and3_Circuit() {
  Boolean* x = new_Boolean(false);
  Boolean* y = new_Boolean(false);
  Boolean* z = new_Boolean(false);
  Boolean** inputs = new_Boolean_array(3);
  inputs[0] = x;
  inputs[1] = y;
  inputs[2] = z;
  
  Boolean* out = new_Boolean(false);
  Boolean** outputs = new_Boolean_array(1);
  outputs[0] = out;
  
  Gate* A1 = new_AndGate();
  Gate* A2 = new_AndGate();
  Gate** gates = new_Gate_array(2);
  gates[0] = A1;
  gates[1] = A2;
  
  Circuit *circuit = new_Circuit(3, inputs, 1, outputs, 2, gates);
  Circuit_connect(circuit, x, Gate_getInput(A1, 0));
  Circuit_connect(circuit, y, Gate_getInput(A1, 1));
  Circuit_connect(circuit, Gate_getOutput(A1), Gate_getInput(A2, 0));
  Circuit_connect(circuit, z, Gate_getInput(A2, 1));
  Circuit_connect(circuit, Gate_getOutput(A2), out);
  return circuit;
}

static Circuit* parta_Circuit(){
  Boolean* x = new_Boolean(false);
  Boolean* y = new_Boolean(false);
  Boolean* z = new_Boolean(false);
  Boolean** inputs = new_Boolean_array(3);
  inputs[0] = x;
  inputs[1] = y;
  inputs[2] = z;

  Boolean* out = new_Boolean(false);
  Boolean** outputs = new_Boolean_array(1);
  outputs[0] = out;

  Gate* A1 = new_AndGate();
  Gate* A2 = new_Inverter();
  Gate* A3 = new_AndGate();
  Gate* A4 = new_OrGate();
  Gate** gates = new_Gate_array(4);
  gates[0] = A1;
  gates[1] = A2;
  gates[2] = A3;
  gates[3] = A4;
 

  Circuit *circuit = new_Circuit(3, inputs, 1, outputs, 4, gates);
  //Gate A2: Inverter | Connect inputs
  Circuit_connect(circuit, y, Gate_getInput(A2, 0));
  //Gate A1: AND | Connect inputs
  Circuit_connect(circuit, x, Gate_getInput(A1, 0));
  Circuit_connect(circuit, Gate_getOutput(A2), Gate_getInput(A1, 1));
  //Gate A3: AND | Connect inputs
  Circuit_connect(circuit, y, Gate_getInput(A3, 0));
  Circuit_connect(circuit, z, Gate_getInput(A3, 1));
  //Gate A4: OR | Connect inputs
  Circuit_connect(circuit, Gate_getOutput(A1), Gate_getInput(A4, 0));
  Circuit_connect(circuit, Gate_getOutput(A3), Gate_getInput(A4, 1));
  //output
  Circuit_connect(circuit, Gate_getOutput(A4), out);
  return circuit;
}

static Circuit* partb_Circuit(){
  Boolean* x = new_Boolean(false);
  Boolean* y = new_Boolean(false);
  Boolean* z = new_Boolean(false);
  Boolean** inputs = new_Boolean_array(3);
  inputs[0] = x;
  inputs[1] = y;
  inputs[2] = z;

  Boolean* out = new_Boolean(false);
  Boolean** outputs = new_Boolean_array(1);
  outputs[0] = out;

  Gate* A1 = new_NandGate();
  Gate* A2 = new_Inverter();
  Gate* A3 = new_NandGate();
  Gate* A4 = new_NorGate();
  Gate** gates = new_Gate_array(4);
  gates[0] = A1;
  gates[1] = A2;
  gates[2] = A3;
  gates[3] = A4;
 

  Circuit *circuit = new_Circuit(3, inputs, 1, outputs, 4, gates);
  //Gate A2: Inverter | Connect inputs
  Circuit_connect(circuit, y, Gate_getInput(A2, 0));
  //Gate A1: NAND | Connect inputs
  Circuit_connect(circuit, x, Gate_getInput(A1, 0));
  Circuit_connect(circuit, Gate_getOutput(A2), Gate_getInput(A1, 1));
  //Gate A3: NAND | Connect inputs
  Circuit_connect(circuit, y, Gate_getInput(A3, 0));
  Circuit_connect(circuit, z, Gate_getInput(A3, 1));
  //Gate A4: NOR | Connect inputs
  Circuit_connect(circuit, Gate_getOutput(A1), Gate_getInput(A4, 0));
  Circuit_connect(circuit, Gate_getOutput(A3), Gate_getInput(A4, 1));
  //output
  Circuit_connect(circuit, Gate_getOutput(A4), out);
  return circuit;
}

static Circuit* partc_Circuit(){
  Boolean* x = new_Boolean(false);
  Boolean* y = new_Boolean(false);
  Boolean** inputs = new_Boolean_array(2);
  inputs[0] = x;
  inputs[1] = y;

  Boolean* z = new_Boolean(false);
  Boolean** outputs = new_Boolean_array(1);
  outputs[0] = z;

  Gate* A = new_AndGate();
  Gate* B = new_Inverter();
  Gate* C = new_Inverter();
  Gate* D = new_AndGate();
  Gate* E = new_OrGate();
  Gate** gates = new_Gate_array(5);
  gates[0] = A;
  gates[1] = B;
  gates[2] = C;
  gates[3] = D;
  gates[4] = E;
 

  Circuit *circuit = new_Circuit(2, inputs, 1, outputs, 5, gates);
  //Gate A
  Circuit_connect(circuit, y, Gate_getInput(A, 0));
  Circuit_connect(circuit, x, Gate_getInput(A, 1));
  //Gate B
  Circuit_connect(circuit, x, Gate_getInput(B, 0));
  //Gate C
  Circuit_connect(circuit, y, Gate_getInput(C, 0));
  //Gate D
  Circuit_connect(circuit, Gate_getOutput(B), Gate_getInput(D, 0));
  Circuit_connect(circuit, Gate_getOutput(C), Gate_getInput(D, 1));
  //Gate E
  Circuit_connect(circuit, Gate_getOutput(A), Gate_getInput(E, 0));
  Circuit_connect(circuit, Gate_getOutput(D), Gate_getInput(E, 1));
  //output
  Circuit_connect(circuit, Gate_getOutput(E), z);
  return circuit;
}

static Circuit* oneBitAdder_Circuit(){
  Boolean* x = new_Boolean(false);
  Boolean* y = new_Boolean(false);
  Boolean* c = new_Boolean(false);
  
  Boolean** inputs = new_Boolean_array(3);
  inputs[0] = x;
  inputs[1] = y;
  inputs[2] = c;

  Boolean* z = new_Boolean(false);
  Boolean* d = new_Boolean(false);
  Boolean** outputs = new_Boolean_array(3);
  outputs[0] = z;
  outputs[1] = d;

  Gate* X = new_Inverter();
  Gate* Y = new_Inverter();
  Gate* Z = new_Inverter();

  Gate* A1 = new_And3Gate();
  Gate* A2 = new_And3Gate();
  Gate* A3 = new_And3Gate();
  Gate* A4 = new_And3Gate();
  Gate* A5 = new_And3Gate();
  Gate* A6 = new_And3Gate();
  Gate* A7 = new_And3Gate();

  Gate* O1 = new_Or4Gate();
  Gate* O2 = new_Or4Gate();
  
  Gate** gates = new_Gate_array(12);
  gates[0] = X;
  gates[1] = Y;
  gates[2] = Z;
  gates[3] = A1;
  gates[4] = A2;
  gates[5] = A3;
  gates[6] = A4;
  gates[7] = A5;
  gates[8] = A6;
  gates[9] = A7;
  gates[10]= O1;
  gates[11]= O2;
  //----------------------------------------

  Circuit *circuit = new_Circuit(3, inputs, 2, outputs, 12, gates);

  //Gate X
  Circuit_connect(circuit, x, Gate_getInput(X, 0));
  //Gate Y
  Circuit_connect(circuit, y, Gate_getInput(Y, 0));
  //Gate Z
  Circuit_connect(circuit, c, Gate_getInput(Z, 0));
  
  //Gate A1
  Circuit_connect(circuit, Gate_getOutput(X), Gate_getInput(A1, 0));
  Circuit_connect(circuit, Gate_getOutput(Y), Gate_getInput(A1, 1));
  Circuit_connect(circuit, c, Gate_getInput(A1, 2));

  //Gate A2
  Circuit_connect(circuit, Gate_getOutput(X), Gate_getInput(A2, 0));
  Circuit_connect(circuit, y, Gate_getInput(A2, 1));
  Circuit_connect(circuit, Gate_getOutput(Z), Gate_getInput(A2, 2));

  //Gate A3
  Circuit_connect(circuit, Gate_getOutput(X), Gate_getInput(A3, 0));
  Circuit_connect(circuit, y, Gate_getInput(A3, 1));
  Circuit_connect(circuit, c, Gate_getInput(A3, 2));

  //Gate A4
  Circuit_connect(circuit, x, Gate_getInput(A4, 0));
  Circuit_connect(circuit, Gate_getOutput(Y), Gate_getInput(A4, 1));
  Circuit_connect(circuit, Gate_getOutput(Z), Gate_getInput(A4, 2));

  //Gate A5
  Circuit_connect(circuit, x, Gate_getInput(A5, 0));
  Circuit_connect(circuit, Gate_getOutput(Y), Gate_getInput(A5, 1));
  Circuit_connect(circuit, c, Gate_getInput(A5, 2));

  //Gate A6
  Circuit_connect(circuit, x, Gate_getInput(A6, 0));
  Circuit_connect(circuit, y, Gate_getInput(A6, 1));
  Circuit_connect(circuit, Gate_getOutput(Z), Gate_getInput(A6, 2));

  //Gate A7
  Circuit_connect(circuit, x, Gate_getInput(A7, 0));
  Circuit_connect(circuit, y, Gate_getInput(A7, 1));
  Circuit_connect(circuit, c, Gate_getInput(A7, 2));

  //Gate O1
  Circuit_connect(circuit, Gate_getOutput(A1), Gate_getInput(O1, 0));
  Circuit_connect(circuit, Gate_getOutput(A2), Gate_getInput(O1, 1));
  Circuit_connect(circuit, Gate_getOutput(A4), Gate_getInput(O1, 2));
  Circuit_connect(circuit, Gate_getOutput(A7), Gate_getInput(O1, 3));
  
  //Gate O2
  Circuit_connect(circuit, Gate_getOutput(A3), Gate_getInput(O2, 0));
  Circuit_connect(circuit, Gate_getOutput(A5), Gate_getInput(O2, 1));
  Circuit_connect(circuit, Gate_getOutput(A6), Gate_getInput(O2, 2));
  Circuit_connect(circuit, Gate_getOutput(A7), Gate_getInput(O2, 3));
  
  
  //output
  Circuit_connect(circuit, Gate_getOutput(O1), z);
  Circuit_connect(circuit, Gate_getOutput(O2), d);
  return circuit;
}

static char* b2s(bool b) {
	return b ? "T" : "F";
}

static void test3In1Out(Circuit* circuit, bool in0, bool in1, bool in2) {
  Circuit_setInput(circuit, 0, in0);
  Circuit_setInput(circuit, 1, in1);
  Circuit_setInput(circuit, 2, in2);
  Circuit_update(circuit);
  bool out0 = Circuit_getOutput(circuit, 0);
  printf("%s %s %s -> %s\n", b2s(in0), b2s(in1), b2s(in2), b2s(out0));
}

static void test2In1Out(Circuit* circuit, bool in0, bool in1) {
  Circuit_setInput(circuit, 0, in0);
  Circuit_setInput(circuit, 1, in1);
  Circuit_update(circuit);
  bool out0 = Circuit_getOutput(circuit, 0);
  printf("%s %s -> %s\n", b2s(in0), b2s(in1), b2s(out0));
}

static void test3In2Out(Circuit* circuit, bool in0, bool in1, bool in2) {
  Circuit_setInput(circuit, 0, in0);
  Circuit_setInput(circuit, 1, in1);
  Circuit_setInput(circuit, 2, in2);
  Circuit_update(circuit);
  bool out0 = Circuit_getOutput(circuit, 0);
  bool out1 = Circuit_getOutput(circuit, 1);
  printf("%s %s %s-> %s %s\n", b2s(in0), b2s(in1), b2s(in2), b2s(out0), b2s(out1));
}


int main(int argc, char **argv) {
  Circuit* circuit = and3_Circuit();
  printf("The and3 circuit (AND of three inputs):\n");
  Circuit_dump(circuit);
  printf("\n");
  printf("Testing: Some input(s) false: should be false\n");
  test3In1Out(circuit, true, true, false);
  printf("Testing: All inputs true: should be true\n");
  test3In1Out(circuit, true, true, true);
  printf("Note: Your program needs to test all possible combinations of input values,\nin order from all false to all true, using a single function.\n");
  Circuit_free(circuit);
  printf("-----------------------part a-----------------------\n");
  Circuit* circuita = parta_Circuit();
  Circuit_dump(circuita);
  test3In1Out(circuita, true, true, true);
  test3In1Out(circuita, true, true, false);
  test3In1Out(circuita, true, false, true);
  test3In1Out(circuita, true, false, false);
  test3In1Out(circuita, false, true, true);
  test3In1Out(circuita, false, true, false);
  test3In1Out(circuita, false, false, true);
  test3In1Out(circuita, false, false, false);
  Circuit_free(circuita);
  printf("----------------------part b------------------------\n");
  Circuit* circuitb = partb_Circuit();
  Circuit_dump(circuitb);
  test3In1Out(circuita, true, true, true);
  test3In1Out(circuita, true, true, false);
  test3In1Out(circuita, true, false, true);
  test3In1Out(circuita, true, false, false);
  test3In1Out(circuita, false, true, true);
  test3In1Out(circuita, false, true, false);
  test3In1Out(circuita, false, false, true);
  test3In1Out(circuita, false, false, false);
  Circuit_free(circuitb);
  printf("----------------------part c------------------------\n");
  Circuit* circuitc = partc_Circuit();
  Circuit_dump(circuitc);
  test2In1Out(circuitc, true, true);
  test2In1Out(circuitc, true, false);
  test2In1Out(circuitc, false, true);
  test2In1Out(circuitc, false, false);
  Circuit_free(circuitc);
  printf("----------------------one bit adder circuit------------------------\n");
  Circuit* onebitadder = oneBitAdder_Circuit();
  Circuit_dump(onebitadder);
  test3In2Out(onebitadder, true, true, true);
  test3In2Out(onebitadder, true, true, false);
  test3In2Out(onebitadder, true, false, true);
  test3In2Out(onebitadder, true, false, false);
  
  test3In2Out(onebitadder, false, true, true);
  test3In2Out(onebitadder, false, true, false);
  test3In2Out(onebitadder, false, false, true);
  test3In2Out(onebitadder, false, false, false);
  
  Circuit_free(onebitadder);
}
